﻿namespace VehiclesAPI.Models
{
    public class Car : Vehicle
    {
        public int NumberOfWheels { get; set; }
    }
}