﻿namespace VehiclesAPI.Models
{
    public class Aircraft : Vehicle
    {
        public int NumberOfEngines { get; set; }
    }
}