﻿namespace VehiclesAPI.Models
{
    public class Engine
    {
        public int Id { get; set; }
        public double Displacement { get; set; }
        public string Producer { get; set; }
        public int? Stroke { get; set; }
    }
}