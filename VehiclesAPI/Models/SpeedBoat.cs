﻿namespace VehiclesAPI.Models
{
    public class SpeedBoat : Vehicle
    {
        public double FullSpeed { get; set; }
    }
}