﻿namespace VehiclesAPI.Models
{
    public enum EngineTypeEnum
    {
        Thermal = 0,
        Electric = 1,
        Reaction = 2
    }
    public class Vehicle
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string FrameNumber { get; set; }
        public string Producer { get; set; }
        public string Plate { get; set; }
        public Engine Motor { get; set; }
        public EngineTypeEnum TypePropulsion { get; set; }
        
    }
}