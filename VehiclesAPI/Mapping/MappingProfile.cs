﻿using AutoMapper;
using AutoMapper.Extensions.EnumMapping;
using VehiclesAPI.Models;
using VehiclesAPI.ModelsDTO;

namespace VehiclesAPI.Mapping
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Vehicle, VehicleDTO>()
                .IncludeAllDerived()
                .ReverseMap();

            CreateMap<Aircraft, AircraftDTO>()
                
                .ReverseMap();

            CreateMap<EngineTypeEnum, AircraftEngineTypeEnum>()
                //.ConvertUsingEnumMapping(opt => opt.MapByName().MapValue(EngineTypeEnum.Thermal, AircraftEngineTypeEnum.Thermal)
                //    .MapValue(EngineTypeEnum.Electric, AircraftEngineTypeEnum.Electric)
                //    .MapValue(EngineTypeEnum.Reaction, AircraftEngineTypeEnum.Reaction))
              .ReverseMap();


            CreateMap<SpeedBoat, SpeedBoatDTO>()
                .ForMember(d=>d.EngineProducer, s=>s.MapFrom(s=>s.Producer))
                    .ReverseMap();

            CreateMap<EngineTypeEnum, SpeedBoatEngineTypeEnum>()
                .ConvertUsingEnumMapping(opt => opt.MapByName().MapValue(EngineTypeEnum.Thermal, SpeedBoatEngineTypeEnum.Thermal)
                .MapValue(EngineTypeEnum.Reaction, SpeedBoatEngineTypeEnum.Reaction))
                .ReverseMap();
                

           // CreateMap<SpeedBoatDTO,Vehicle>()
             //   .ForMember(d=>d.Motor,opt=>opt.MapFrom(d=> { return new Engine() {Displacement = d.TempiMotore, Producer= d.}})
        }
    }
}