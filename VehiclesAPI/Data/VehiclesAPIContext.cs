﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using VehiclesAPI.Models;

namespace VehiclesAPI.Data
{
    public class VehiclesAPIContext : DbContext
    {
        public VehiclesAPIContext (DbContextOptions<VehiclesAPIContext> options)
            : base(options)
        {
        }

        public DbSet<VehiclesAPI.Models.Vehicle> Vehicles { get; set; }
        public DbSet<VehiclesAPI.Models.Aircraft> Aircrafts { get; set; }
        public DbSet<VehiclesAPI.Models.SpeedBoat> SpeedBoats { get; set; }
        public DbSet<VehiclesAPI.Models.Car> Cars { get; set; }
        public DbSet<VehiclesAPI.Models.Engine> Engines { get; set; }
    }
}
