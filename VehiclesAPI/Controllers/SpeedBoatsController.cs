﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using VehiclesAPI.Data;
using VehiclesAPI.Models;
using VehiclesAPI.ModelsDTO;

namespace VehiclesAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SpeedBoatsController : ControllerBase
    {
        private readonly VehiclesAPIContext _context;
        private readonly IMapper _mapper;
        public SpeedBoatsController(VehiclesAPIContext context,IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/SpeedBoats
        [HttpGet]
        public async Task<ActionResult<IEnumerable<SpeedBoatDTO>>> GetSpeedBoats()
        {
            var speedboats= await _context.SpeedBoats.ToListAsync();
            
            return _mapper.Map<List<SpeedBoatDTO>>(speedboats);

        }

        // GET: api/SpeedBoats/5
        [HttpGet("{id}")]
        public async Task<ActionResult<SpeedBoat>> GetSpeedBoat(int id)
        {
            var speedBoat = await _context.SpeedBoats.FindAsync(id);

            if (speedBoat == null)
            {
                return NotFound();
            }

            return speedBoat;
        }

        // PUT: api/SpeedBoats/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutSpeedBoat(int id, SpeedBoat speedBoat)
        {
            if (id != speedBoat.Id)
            {
                return BadRequest();
            }

            _context.Entry(speedBoat).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SpeedBoatExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/SpeedBoats
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<SpeedBoatDTO>> PostSpeedBoat(SpeedBoatDTO speedBoatDTO)
        {
            var speedboat = _mapper.Map<SpeedBoat>(speedBoatDTO);
            _context.SpeedBoats.Add(speedboat);
            await _context.SaveChangesAsync();
            

            return CreatedAtAction("GetSpeedBoat", new { id = speedboat.Id }, speedboat);
        }

        // DELETE: api/SpeedBoats/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteSpeedBoat(int id)
        {
            var speedBoat = await _context.SpeedBoats.FindAsync(id);
            if (speedBoat == null)
            {
                return NotFound();
            }

            _context.SpeedBoats.Remove(speedBoat);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool SpeedBoatExists(int id)
        {
            return _context.SpeedBoats.Any(e => e.Id == id);
        }
    }
}
