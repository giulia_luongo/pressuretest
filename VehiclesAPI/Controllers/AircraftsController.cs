﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using VehiclesAPI.Data;
using VehiclesAPI.Models;
using VehiclesAPI.ModelsDTO;

namespace VehiclesAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AircraftsController : ControllerBase
    {
        private readonly VehiclesAPIContext _context;
        private readonly IMapper _mapper;

        public AircraftsController(VehiclesAPIContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Aircrafts
        [HttpGet]
        public async Task<ActionResult<IEnumerable<AircraftDTO>>> GetAircrafts()
        {
            var aircrafts = await _context.Aircrafts.ToListAsync();
            return _mapper.Map<List<AircraftDTO>>(aircrafts);
        }

        // GET: api/Aircrafts/5
        [HttpGet("{id}")]
        public async Task<ActionResult<AircraftDTO>> GetAircraft(int id)
        {
            var aircraft = await _context.Aircrafts.FindAsync(id);

            if (aircraft == null)
            {
                return NotFound();
            }

            return _mapper.Map<AircraftDTO>(aircraft);
        }

        // PUT: api/Aircrafts/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<ActionResult<AircraftDTO>> PutAircraft(int id, AircraftDTO aircraftDTO)
        {
            var aircraft = _mapper.Map<Aircraft>(aircraftDTO);
            if (id != aircraft.Id)
            {
                return BadRequest();
            }

            _context.Entry(aircraft).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AircraftExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Ok(_mapper.Map<AircraftDTO>(aircraft));
        }

        // POST: api/Aircrafts
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<AircraftDTO>> PostAircraft(AircraftDTO aircraftDTO)
        {
            var aircraft = _mapper.Map<Aircraft>(aircraftDTO);
            _context.Aircrafts.Add(aircraft);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetAircraft", new { id = aircraft.Id }, _mapper.Map<AircraftDTO>(aircraft));
        }

        // DELETE: api/Aircrafts/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAircraft(int id)
        {
            var aircraft = await _context.Aircrafts.FindAsync(id);
            if (aircraft == null)
            {
                return NotFound();
            }

            _context.Aircrafts.Remove(aircraft);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool AircraftExists(int id)
        {
            return _context.Aircrafts.Any(e => e.Id == id);
        }
    }
}
