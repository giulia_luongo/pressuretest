﻿namespace VehiclesAPI.ModelsDTO
{
    public class VehicleDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string FrameNumber { get; set; }
        public string Producer { get; set; }
        public string Plate { get; set; }
    }
}