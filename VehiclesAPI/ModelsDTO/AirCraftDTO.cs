﻿namespace VehiclesAPI.ModelsDTO
{
    public enum AircraftEngineTypeEnum
    {
        Thermal = 0,
        Electric = 1,
        Reaction = 2
    }
    public class AircraftDTO : VehicleDTO
    {
        public int NumberOfEngines { get; set; }
        public AircraftEngineTypeEnum TypePropulsion { get; set; }
        
    }
}