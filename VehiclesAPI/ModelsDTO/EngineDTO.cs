﻿namespace VehiclesAPI.ModelsDTO
{
    public class EngineDTO
    {
        public int Id { get; set; }
        public double Cilindrata { get; set; }
        public string Produttore { get; set; }
        public int? Tempi { get; set; }
        
    }
}