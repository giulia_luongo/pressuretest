﻿namespace VehiclesAPI.ModelsDTO
{
    public enum SpeedBoatEngineTypeEnum
    {
        Thermal = 0,
        Reaction = 2
    }
    public class SpeedBoatDTO : VehicleDTO
    {
        public double CilindrataMotore { get; set; }
        public string EngineProducer { get; set; }
        public int TempiMotore { get; set; }
        public double FullSpeed { get; set; }
        public SpeedBoatEngineTypeEnum TypePropulsion { get; set; }
        
    }
}