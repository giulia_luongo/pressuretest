﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VehiclesAPI.ModelsDTO
{
    public enum CarEngineTypeEnum
    {
        Thermal = 0,
        Electric = 1,
        Reaction = 2
    }
    public class CarEngineDTO : EngineDTO
    {

        public CarEngineTypeEnum TypePropulsion { get; set; }
    }
}
