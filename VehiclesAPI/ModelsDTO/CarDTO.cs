﻿namespace VehiclesAPI.ModelsDTO
{
    public class CarDTO : VehicleDTO
    {
        public int NumberOfWheels { get; set; }
        public CarEngineDTO Motor { get; set; }
    }
}