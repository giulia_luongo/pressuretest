﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VehiclesAPI.Data;
using VehiclesAPI.Models;

namespace VehiclesApi.IntegrationTest
{
    class Utilities
    {
        public static void InitializeDbForTests(VehiclesAPIContext db) {
            db.Aircrafts.AddRange();
            db.SaveChanges();
        }
        public static void ReinitializeDbForTests(VehiclesAPIContext db)
        {
            db.Aircrafts.RemoveRange(db.Aircrafts);
            InitializeDbForTests(db);
        }

        public static IEnumerable<Aircraft> GetSeedingAircrafts()
        {
            return new List<Aircraft>
            {
                new Aircraft()
                {
                    Name = "Velivolo1",
                    FrameNumber = "1",
                    Producer = "Producer1",
                    Plate = "ab123cd",
                    Motor = new Engine()
                    {
                        Displacement = 1.5,
                        Producer = "Yamaha",
                        Stroke = 10
                    },
                    TypePropulsion = EngineTypeEnum.Thermal,
                    NumberOfEngines = 2
                },
                new Aircraft()
                {
                    Name = "Velivolo2",
                    FrameNumber = "2",
                    Producer = "Producer2",
                    Plate = "ab456cd",
                    Motor = new Engine()
                    {
                        Displacement = 1.8,
                        Producer = "BMW",
                        Stroke = 8
                    },
                    TypePropulsion =EngineTypeEnum.Reaction,
                    NumberOfEngines = 4
                },
                new Aircraft()
                {
                    Name = "Velivolo3",
                    FrameNumber = "3",
                    Producer = "Producer3",
                    Plate = "ab333cd",
                    Motor = new Engine()
                    {
                        Displacement = 2.8,
                        Producer = "Ferrari",
                        Stroke = 12
                    },
                    TypePropulsion = EngineTypeEnum.Electric,
                    NumberOfEngines = 9
                }
            };
        }
    }
}
